import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { ListContainer } from './list.container';
import { NO_ERRORS_SCHEMA, SimpleChange, Component } from '@angular/core';
import { By } from '@angular/platform-browser';


@Component({
  selector: 'Test-App',
  template: `
   <div tree-node [arborescence]="arborescence"></div>
  `
})
class TestApp {
  arborescence = { 
    code: 'C01.00.00', label:'uneCirconstanceDeNiveau1', niveau: 1, enfants: [{
      code: 'C01.01.00', label:'uneCirconstanceDeNiveau2', niveau: 2, enfants: [{
        code: 'C01.01.01', label:'uneCirconstanceDeNiveau3', niveau: 3
      },
      {
        code: 'C01.01.02', label:'uneCirconstanceDeNiveau3', niveau: 3
      }]
    }]
  }
}

describe('ListRadioContainer', () => {
  let fixture: ComponentFixture<TestApp>
  let component : TestApp
  let mockApp: TestApp

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListContainer,
        TestApp
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(TestApp);
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  describe('Avec un dictionnaire en entrée', () => {
    
    describe('Comportement initiale de l`arbre', () => {
      it('n`affiche que la liste des noeuds du niveau 1 de l`arborescence', () => {
        console.log(component.arborescence)
        // Then
        expect(fixture.debugElement.queryAll(By.css('.menu-niveau-1')).length).toBe(1)
        expect(fixture.debugElement.queryAll(By.css('.menu-niveau-1'))[0].nativeElement.textContent).toBe('C01.00.00 uneCirconstanceDeNiveau1 ')
      })
      
     /* describe('Au click sur un noeud du niveau 1', () => {
        beforeEach(() => {
          fixture.debugElement.queryAll(By.css('.menu-niveau-1'))[0].nativeElement.click()
        })
        it('affiche les enfants du niveau', () => {
          expect(fixture.debugElement.queryAll(By.css(".menu-niveau-2")).length).toBe(1)
        })
      })*/
    })
    
    
  })
});
