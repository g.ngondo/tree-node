import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ListContainer } from './list.container';

@NgModule({
  declarations: [
    ListContainer
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  bootstrap: [ListContainer]
})
export class AppModule { }
