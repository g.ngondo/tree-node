import { Component, Input, ViewChildren, ContentChildren, QueryList, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: '[tree-node]',
  template: `
  {{arborescence && arborescence.label}}
  <button (click)="toggleLaffichageDesEnfants()"></button>
  <ul class="menu-niveau-{{arborescence.niveau}}" *ngIf="peutAfficherLesEnfants()">
    <li *ngFor="let enfant of this.arborescence.enfants" tree-node [arborescence]="enfant"></li>
  </ul>
  `
})
export class ListContainer implements OnChanges {

  ngOnChanges(changes: SimpleChanges): void {
    const arborescenceChange = changes.arborescence
    if (arborescenceChange && arborescenceChange.currentValue !== arborescenceChange.previousValue && arborescenceChange.currentValue) {
      this.arborescence = arborescenceChange.currentValue
    }
  }
  afficheLesEnfants = false

  @Input()
  arborescence: Arborescence 

  @ContentChildren(ListContainer)
  enfants: QueryList<ListContainer>

  toggleLaffichageDesEnfants() {
    this.afficheLesEnfants = !this.afficheLesEnfants
  }

  peutAfficherLesEnfants() {
    return this.afficheLesEnfants
  }
}

export class Arborescence {
  code: string 
  label: string
  niveau: number
  enfants?: Arborescence[] | undefined
}